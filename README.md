<!--
 -------------------------------------------------------------------------------
 MIT License

 Copyright (c) 2024 University College Cork (UCC)
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
  
   
 SPDX-License-Identifier: MIT
   
 Contributors:
    Panagiotis Kyziropoulos - Author
    Dimitrios Bikoulis - Co-author
 
 ------------------------------------------------------------------------------- 
-->
# Fairness Data Module

## Roadmap M13-M18

![Roadmap](/images/Roadmap_M13-M18.png "Roadmap")

## To Do List M13-M18

- [X] Issues definition
- [X] Initial framework setting for the web app
- [X] SQL configuration: Projects
- [X] Back End v0.1 
- [X] Incorporation of descriptive statistics for Projects (DScomponent v1.0)
- [X] Functional Proto-front-end (for demo)
- [X] Incorporation of fairness metrics for Projects (Mcomponent v1.0)
- [X] Functional Swagger

## To Do List M19-M25

- [ ] Verify and update requirements
- [ ] Code cleaning
- [ ] Minor bugs
- [ ] Update HTML content with fairness information
- [ ] Complete documentation

![architecture_Fairness](/images/arch_extended_Fairnessbias.png)

## Module purpose

Data bias fairness refers to the principle of ensuring that datasets used in decision-making processes are 
representative and devoid of biases that could lead to unfair or discriminatory outcomes. 
It involves identifying, mitigating, and preventing biases in data collection, processing, and analysis to promote equitable treatment and outcomes for all individuals or groups affected by the decisions made using that data.
The module is meant to provide a *supporting tool* for data bias handling within datasets used in DATAMITE. 
This concepts aligns with the broader framework of data quality, being a crucial part of the DATAMITE's value proposition.

For addressing the data perspective of fairness, sensitive or protected attributes must be explicited, to which the decision making process should not be attached. Common examples of sensitive attributes are Gender, Ethnicity, Religion, Race, and Age, to just name a few. Given a selected sensitive attribute, fairness metrics help to quantify and analyze potential biases or disparities in outcomes. Examples of fairness metrics include disparate impact, equalized odds, and statistical parity difference. In cases where the outcome of the fairness analysis is not as expected, the user are provided with additional techniques and tool that help data bias mitigation. Some approaches focus on the target variable
used for the decision making process, while other techniques focus on adjusting the whole distributions according to the
notion of fairness to be enforced. 

The following schema illustrates a systematic process for implementing a supportive tool, as flow chart. 

![Flow chart](/images/T34-fairness-flow.png "Fairness assessment flow chart")

## Architecture

The Fairness Data module emcompasses an standalone service for DATAMITE user. The architecture of the service is modular, and it is divided in five components.

1. Descriptive Statistics component, which provides a set of mathematical tools and pre-processing techniques useful for
the preparation and analysis of the data.

2. Metrics component, containing a compendium of the most relevant Fairness metrics available in the literature, for data bias assesment.

3. Mitigation Techniques component, containing a set of well established techniques with which the user can mitigate the fairness bias in their data.

4. Fairness Service front end.

5. Fairness Service back end.

The following schema show the interoperability between the different components.

![Flow chart](/images/T34-architecture.png "Fairness assessment flow chart")