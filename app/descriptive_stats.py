# -------------------------------------------------------------------------------
# MIT License
   
# Copyright (c) 2024 University College Cork (UCC)
    
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
    
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
   
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
     
      
# SPDX-License-Identifier: MIT
      
# Contributors:
#    Panagiotis Kyziropoulos - Author
#    Dimitrios Bikoulis - Co-author
    
# ------------------------------------------------------------------------------- 

# detection/descriptive_stats.py
import pandas as pd
import numpy as np
import itertools
from scipy.stats import chi2_contingency
from sklearn.metrics import mutual_info_score
from app.tools import type_check,_check_attribute

# ------------------- Application functions ---------------------------#

def get_description(file_path: str):
    try:
        dataset = pd.read_csv(file_path)
        description_results = analyse_dataset(dataset)
        return description_results
    except:
        return False

def get_proportions(file_path: str, attribute_name: str):
    try:
        dataset = pd.read_csv(file_path)
        description_results = proportions(dataset, attribute_name)
        return description_results
    except:
        return False

def get_distros(file_path: str, class_name: str, sensitive_name: str):
    try:
        dataset = pd.read_csv(file_path)
        results = outcome_distribution_by_group(dataset, class_name, sensitive_name)
        return results
    except:
        return False

# ------------------- Statistical functions ---------------------------#

@type_check
def _cramers_v(confusion_matrix: pd.DataFrame) -> float:
    chi2 = chi2_contingency(confusion_matrix)[0]
    n = confusion_matrix.sum().sum()
    phi2 = chi2 / n
    r, k = confusion_matrix.shape
    phi2corr = max(0, phi2 - ((k-1)*(r-1)) / (n-1))
    rcorr = r - ((r-1)**2) / (n-1)
    kcorr = k - ((k-1)**2) / (n-1)
    return np.sqrt(phi2corr / min((kcorr-1), (rcorr-1)))

@type_check
def analyse_dataset(data: pd.DataFrame, threshold:int = 50):
    result=[]
    beauty_list = []
    for column in data.columns:
        dtype = data[column].dtype
        unique_count = data[column].nunique()
        if (dtype == "object"):
            if (unique_count <= threshold):
                column_type = "Categorical/Ordinal"
                values=data[column].unique()
            else:
                column_type = "Text"
                values="-"   
        if (np.issubdtype(dtype, np.number)):
            if (unique_count <= threshold):
                column_type = "Categorical/Ordinal"
                values=data[column].unique()
            else:
                column_type = "Continuous"
                values="-"          
        if (np.issubdtype(dtype, np.number) or np.issubdtype(dtype, np.bool_) or (dtype == "object")) and data[column].nunique() == 2:
            column_type = "Binary"
            values=data[column].unique()
        if (dtype == "object"):
            dtype = "text"
        result.append({
            "Column Name": column,
            "Data Type": dtype,
            "Column Type (suggestion)": column_type,
            "Number_Values":unique_count,
            "Values":values
        })
        beauty_list.append([column,column_type,unique_count,list(values)])
    return beauty_list #result

@type_check
def proportions(data: pd.DataFrame, attribute: str) -> dict:
    # return object
    result={}
    # check validity of features
    _check_attribute(data,attribute)
    result = data[attribute].value_counts(normalize=True).to_dict()
    return result

@type_check
def outcome_distribution_by_group(data: pd.DataFrame, class_attribute: str, sensitive_attribute: str) -> dict:
    # return object
    result={}
    # check validity of features
    _check_attribute(data,class_attribute)
    _check_attribute(data,sensitive_attribute)     
    temp=data.groupby(sensitive_attribute)[class_attribute].value_counts(normalize=True).to_dict()
    # iterate through values
    for label in temp:
        if label[0] not in result:
            result[label[0]]={
                label[1]:temp[label]
            }
        else:
            result[label[0]][label[1]]=temp[label]
    return result

@type_check
def contingency(data: pd.DataFrame, class_attribute: str, sensitive_attributes: list,alpha: float = 0.05, verbose: bool = False) -> list:
    # form list of attributes
    attributes=[]
    for attribute in sensitive_attributes:
        if not (attribute in data):
            print("Sensitive attribute value is not part of the data.")
            return None
        attributes.append(attribute)
    if not (class_attribute in data):
        print("Class attribute value is not part of the data.")
        return None
    attributes.append(class_attribute)
    #get combinations without replacement 
    one_way_combinations = list(itertools.combinations(attributes, 2))
    contingency=[]
    for comb in one_way_combinations:
        attr1=comb[0]
        attr2=comb[1]
        # Create a contingency table
        contingency_table = pd.crosstab(data[attr1], data[attr2])
        # Perform the Chi-squared test
        chi2, p, dof, expected = chi2_contingency(contingency_table)     
        # Cramers_v test
        cramer_v = _cramers_v(contingency_table)   
        # form result list
        contingency.append({
            "attribute1":attr1,
            "attribute2":attr2,
            "contingency_table":contingency_table.to_dict(),
            "chi2":chi2,
            "cramers_v":cramer_v,
            "p":p,
            "dof":dof,
            "expected":dict(enumerate(expected.flatten(), 1)),
        })
        # Print the results
        if (verbose):
            print(f"\nAssociation between {attr1} and {attr2}.")
            print("Contingency Table:")
            print(contingency_table)
            print("\nChi-squared statistic:", chi2)
            print("Cramer's V:", cramer_v)
            print("Degrees of Freedom:", dof)
            print("p-value:", p)
            # Check for statistical significance
            if p < alpha:
                print(f"There is a statistically significant association between {attr1} and {attr2}.")
            else:
                print(f"There is no statistically significant association between {attr1} and {attr2}.")
    return contingency

@type_check
def mutual_information(data: pd.DataFrame, class_attribute: str, sensitive_attributes: list,verbose: bool = False) -> list:
    # form list of attributes
    attributes=[]
    for attribute in sensitive_attributes:
        if not (attribute in data):
            print("Sensitive attribute value is not part of the data.")
            return None
        attributes.append(attribute)
    if not (class_attribute in data):
        print("Class attribute value is not part of the data.")
        return None
    attributes.append(class_attribute)
    #get combinations without replacement 
    one_way_combinations = list(itertools.combinations(attributes, 2))
    mutual_information=[]
    for comb in one_way_combinations:
        attr1=comb[0]
        attr2=comb[1]
        # Calculate Mutual Information
        mi = mutual_info_score(data[attr1], data[attr2])
        # form result list
        mutual_information.append({
            "attribute1":attr1,
            "attribute2":attr2,
            "mi":mi,
        })
        # Print the result
        if (verbose):
            print(f"Mutual Information between {str(attr1)} and {str(attr2)}: "+str(mi))
    return mutual_information
