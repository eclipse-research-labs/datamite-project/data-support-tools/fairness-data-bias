# -------------------------------------------------------------------------------
# MIT License
   
# Copyright (c) 2024 University College Cork (UCC)
    
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
    
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
   
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
     
      
# SPDX-License-Identifier: MIT
      
# Contributors:
#    Panagiotis Kyziropoulos - Author
#    Dimitrios Bikoulis - Co-author
    
# ------------------------------------------------------------------------------- 

from flask import Flask
from flask_cors import CORS
import logging
from logging.handlers import RotatingFileHandler
from app.models import db
from flasgger import Swagger

# initialize App
app = Flask(__name__)
CORS(app)

# initialize Swagger API
swagger = Swagger(app, template_file="swagger/api_specification.yaml")

# # Configure logging
# formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
# handler = RotatingFileHandler('logs/app.log', maxBytes=10000, backupCount=1)
# handler.setLevel(logging.DEBUG)  # Set the log level to DEBUG to log all messages
# handler.setFormatter(formatter)
# app.logger.addHandler(handler)

# Choose configuration setting
app.config.from_object('config.development')

# initialize
db.init_app(app)

# Setup Database
with app.app_context():
    db.create_all()