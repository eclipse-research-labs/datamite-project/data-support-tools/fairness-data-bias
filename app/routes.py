# -------------------------------------------------------------------------------
# MIT License
   
# Copyright (c) 2024 University College Cork (UCC)
    
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
    
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
   
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
     
      
# SPDX-License-Identifier: MIT
      
# Contributors:
#    Panagiotis Kyziropoulos - Author
#    Dimitrios Bikoulis - Co-author
    
# ------------------------------------------------------------------------------- 

from app import app
from flask import jsonify, request, render_template_string, render_template
from werkzeug.utils import secure_filename
import markdown2
import os
import app.services as sv
import app.models as ml
import app.descriptive_stats as ds
import app.metrics as met
# import app.descriptive_stats as ds
import ast, json

#---------------------------------- Projects display API 
@app.route('/')
def home():
    if request.method == "GET":
        try:
            projects = sv.get_projects()
            return render_template("index.html",projects=projects)
        except:
            return jsonify({'message': 'Unavailable database'}), 503
    
    
@app.route('/api/<project_id>', methods=['GET','POST'])
def display_project(project_id):
    if request.method == "GET":
        try:
            projects = sv.get_projects()
        except:
            return jsonify({'message': 'Unavailable database'}), 503
        
        if project_id in [project.id for project in projects]:
            selected = sv.get_project_with_id(project_id)
            with open(os.path.join(app.config['UPLOAD_FOLDER'],selected.config_path), 'r', encoding='utf-8') as file:
                file_content = file.read()
            dictionary_data = ast.literal_eval(file_content)
            return render_template("displayed_project.html",projects=projects,selected=selected,data=dictionary_data)
        else:
            return jsonify({'message': 'Project not found'}), 404


#---------------------------------- Project management in database APIs

# GET: get_projects
# Description: The user can retrieve all the project created in the App
@app.route('/api/get_projects', methods=['GET'])
def get_projects():
    try:
        projects = sv.get_projects()
        resp=[]
        for project in projects:
            resp.append({
                "id":project.id,
                "name":project.name,
                "dataset_path":project.dataset_path,
                "config_path":project.config_path,
                "date":project.date
            })
        return jsonify(resp), 200
    except:
        return jsonify({'message': 'Projects were not retrieved'}), 500

# GET: get_project
# Description: The user can retrieve a project by providing the Project's ID
@app.route('/api/get_project/<string:project_id>', methods=['GET'])
def get_project(project_id):
    try:
        project = sv.get_project_with_id(project_id)
        if project is None:
            return jsonify({'message': 'Project was not retrieved (Invalid ID)'}), 422
        else:
            return jsonify({
                'id': project.id, 
                'name': project.name, 
                'dataset_path': project.dataset_path, 
                'config_path': project.config_path, 
                'date': project.date}), 200
    except:
        return jsonify({'message': 'Project was not retrieved (Invalid ID)'}), 422

# POST: add_project
# Description: The user can add a new project by providing a new Project Name
@app.route('/api/add_project', methods=['POST'])
def add_project():
    try:
        project_name = request.form.get('name')
        project_description = request.form.get('description')
        new_project_id = sv.add_project(project_name,project_description,app.config['UPLOAD_FOLDER'])
    except:
        return jsonify({'message': 'Project was not added'}), 422
    if 'file' in request.files:
        file = request.files['file']
        if file.filename == '':
            return jsonify({'message': 'File name invalid'}), 400    
        answer = sv.upload_file_to_project(new_project_id,file)
        if answer == 422:
            return jsonify({'message':'Project already has a datafile. Create new project'}), 422
        elif answer == 401:
            return jsonify({'message':'This error, damn'}), 401
        elif answer == 413:
            return jsonify({'message':'File size exceeds the limit'}), 413
        elif answer == 400:
            return jsonify({'error':"File not uploaded"}), 422
        try:
            sv.update_configuration_file(new_project_id,file)
            return jsonify({'success': f"Project {project_name} added successfully, with file"}), 200
        except:
            return jsonify({'error':"File uploaded to project, not to configuration file"}), 422             
    return jsonify({'message': f"Project {project_name} added successfully, without file"}), 200

# POST: remove_project
# Description: The user can delete a project by providing the Project's ID
@app.route('/api/remove_project/<string:project_id>', methods=['DELETE'])
def remove_project(project_id):
    try:
        ret = sv.remove_project(project_id,app.config['UPLOAD_FOLDER'])
        if ret!=1:
            return jsonify({'message': 'Project was not deleted (Invalid ID)'}), 422
        else:
            return jsonify({'message': 'Project removed successfully'}), 200
    except:
        return jsonify({'message': 'Project was not deleted (Invalid ID)'}), 422

# POST: upload
# Description: The user can upload a file (attach to new project using project name optional)
@app.route('/api/upload', methods=['POST'])
def upload_file():
    if 'file' not in request.files:
        return jsonify({'message': 'No file uploaded'}), 422
    file = request.files['file']
    if file.filename == '':
        return jsonify({'message': 'File not uploaded'}), 400

    # Get additional data from form fields
    project_id = request.form.get('project_id')
    try:
        project = sv.get_project_with_id(project_id)
    except:
        return jsonify({'message': 'Project was not retrieved (Invalid ID)'}), 422
    try:
        answer = sv.upload_file_to_project(project_id,file)
        if answer == 422:
            return jsonify({'message':'Project already has a datafile. Create new project'}), 422
        elif answer == 413:
            return jsonify({'message':'File size exceeds the limit'}), 413
        elif answer == 400:
            return jsonify({'error':"File not uploaded"}), 422 
    except:
        return jsonify({'error': 'File not uploaded'}), 422
    try:
        sv.update_configuration_file(project_id,file)
        return jsonify({'success': 'File uploaded successfully to project'}), 200
    except:
        return jsonify({'error':"File uploaded to project, not to configuration file"}), 422

#---------------------------------- FAIRNESS APIs

@app.route('/api/give_stats',methods=['POST'])
def give_db_stats():
    data = request.json
    project_id = data.get('project_id')
    print(project_id)
    try:
        projects = sv.get_projects()
    except:
        return jsonify({'message': 'STATS - Unavailable database'}), 503
    # Checking that the project id input is valid
    if project_id in [project.id for project in projects]:
        selected = sv.get_project_with_id(project_id)
    else:
        return jsonify({'message': 'STATS - Project not found'}), 404
    # Checking dataset is available and getting the descriptive statistics
    file_path = os.path.join(app.config['UPLOAD_FOLDER'], selected.dataset_path)
    if os.path.isfile(file_path):
        try:
            answer = ds.get_description(file_path)
            if not answer:
                return jsonify({'message': 'STATS - Dataset found but not processed: wrong type of file?'}), 422         
        except:
            return jsonify({'message': 'STATS - Dataset found but not processed: wrong type of file?'}), 422
    else:
        app.logger.error('error: README.md file was not found.')
        return jsonify({'error': 'STATS - File not found'}), 400         
    # Checking that configuration file is available
    try:
        with open(os.path.join(app.config['UPLOAD_FOLDER'], selected.config_path), 'r', encoding='utf-8') as file:
            file_content = file.read()
    except FileNotFoundError:
        return jsonify({'message': 'STATS - File not found'}), 404  
    except SyntaxError:
        print("Invalid syntax in the file. Make sure the file contains a valid Python dictionary.")
        return jsonify({'message': 'STATS - Corrupted file'}), 422
    # Dropping the results in the json file
    try:
        dictionary_data = ast.literal_eval(file_content)
        dictionary_data['Results']['stats']= answer     
        with open(os.path.join(app.config['UPLOAD_FOLDER'], selected.config_path), 'w', encoding='utf-8') as file:
            json.dump(dictionary_data, file, default=str)
        return jsonify({'message':f'Statistics updated for the project {selected.name}.'}), 200
    except IOError:
        print("Error saving dictionary to file.")
        return jsonify({'message': 'STATS - Internal Server Error while saving file'}), 500
    
@app.route('/api/give_proportions',methods=['POST'])
def give_db_props():

    # Checking if database is available
    try:
        projects = sv.get_projects()
    except:
        return jsonify({'message': 'PROPS - Unavailable database'}), 503    
    
    # Getting post data
    data = request.json
    project_id = data.get('project_id')
    attribute_name = data.get('attribute')

    # Checking all good with the selected project
    if project_id in [project.id for project in projects]:
        selected = sv.get_project_with_id(project_id)
    else:
        return jsonify({'message': 'PROPS - Project not found'}), 404
    if selected.dataset_path == '':
        return jsonify({'message': 'PROPS - The project does not have an associated dataset'}), 422
    else:
        dataset_name = selected.dataset_path

    # Checking dataset is available and computing the proportions
    file_path = os.path.join(app.config['UPLOAD_FOLDER'], dataset_name)
    if os.path.isfile(file_path):
        try:
            answer = ds.get_proportions(file_path,attribute_name)
            if not answer:
                return jsonify({'message': 'PROPS - Dataset found for does not allow function: wrong attribute'}), 422         
        except:
            return jsonify({'message': 'PROPS - Dataset found but not processed'}), 422
    else:
        app.logger.error('error: the configuration file was not found.')
        return jsonify({'message': 'PROPS - Configuration file not found'}), 404 

    # Checking that configuration file is available
    try:
        with open(os.path.join(app.config['UPLOAD_FOLDER'], selected.config_path), 'r', encoding='utf-8') as file:
            file_content = file.read()
    except FileNotFoundError:
        return jsonify({'message': 'PROPS - Configuration file not found 2'}), 404  
    except SyntaxError:
        print("Invalid syntax in the file. Make sure the file contains a valid Python dictionary.")
        return jsonify({'message': 'PROPS - Corrupted file'}), 422

    # Dropping the results in the configuration json file
    try:
        dictionary_data = ast.literal_eval(file_content)
        dictionary_data['Results']['props']= [attribute_name,answer]     
        with open(os.path.join(app.config['UPLOAD_FOLDER'], selected.config_path), 'w', encoding='utf-8') as file:
            json.dump(dictionary_data, file, default=str)
        return jsonify({'message':f'Proportions computed for the attribute {attribute_name}.'})
        #jsonify({'Message': 'Results uploaded'}), 200 #Remove this with frontend
    except IOError:
        print("Error saving dictionary to file.")
        return jsonify({'message': 'PROPS - Internal Server Error while saving file'}), 500

@app.route('/api/distbygroup',methods=['POST'])
def give_distribution_by_group():

    # Checking if database is available
    try:
        projects = sv.get_projects()
    except:
        return jsonify({'message': 'DIST - Unavailable database'}), 503 
       
    # Getting API data
    data = request.json
    project_id = data.get('project_id')
    class_name = data.get('class')
    sensitive_name = data.get('sensitive')

    # Checking project is ok
    if project_id in [project.id for project in projects]:
        selected = sv.get_project_with_id(project_id)
    else:
        return jsonify({'message': 'DIST - Project not found'}), 404
    if selected.dataset_path == '':
        return jsonify({'message': 'DIST - The project does not have an associated dataset'}), 422
    else:
        dataset_name = selected.dataset_path

    # Checking dataset is available and computing the distributions
    file_path = os.path.join(app.config['UPLOAD_FOLDER'], dataset_name)
    if os.path.isfile(file_path):
        try:
            answer = ds.get_distros(file_path,class_name,sensitive_name)
            if not answer:
                return jsonify({'message': 'DIST - Dataset found for does not allow function: wrong attribute'}), 422         
        except:
            return jsonify({'message': 'DIST - Dataset found but not processed'}), 422
    else:
        app.logger.error('error: README.md file was not found.')
        return jsonify({'error': 'DIST - File not found'}), 400 
    
    # Checking that configuration file is available
    try:
        with open(os.path.join(app.config['UPLOAD_FOLDER'], selected.config_path), 'r', encoding='utf-8') as file:
            file_content = file.read()
    except FileNotFoundError:
        return jsonify({'message': 'DIST - File not found'}), 404  
    except SyntaxError:
        print("Invalid syntax in the file. Make sure the file contains a valid Python dictionary.")
        return jsonify({'message': 'DIST - Corrupted file'}), 422    
    
    # Dropping the results in the json file
    try:
        dictionary_data = ast.literal_eval(file_content)
        dictionary_data['Results']['distros']= [class_name,sensitive_name,answer]     
        with open(os.path.join(app.config['UPLOAD_FOLDER'], selected.config_path), 'w', encoding='utf-8') as file:
            json.dump(dictionary_data, file, default=str)
        return jsonify({'message':f'Distributions by group computed for class {class_name} and sensitive attribute {sensitive_name}.'}), 200
        #jsonify({'Message': 'Results uploaded'}), 200 #Remove this with frontend
    except IOError:
        print("Error saving dictionary to file.")
        return jsonify({'message': 'DIST - Internal Server Error'}), 500

@app.route('/api/metrics',methods=['POST'])
def give_fairness_metric():
    print('in metric compy')
    # Checking if database is available
    try:
        projects = sv.get_projects()
    except:
        return jsonify({'message': 'MET - Unavailable database'}), 503 
       
    # Getting API data
    data = request.json
    project_id = data.get('project_id')
    class_name = data.get('class')
    positive_outcome = data.get('positive')
    sensitive_name = data.get('sensitive')
    priviledged_group = data.get('priviledged')
    metric = data.get('metric')
    if metric == 'Statistical Parity':
        metcode = 'parity'
    elif metric == 'Disparate Impact':
        metcode = 'disimp'
    elif metric == 'Stats':
        metcode = 'stats'
    # Checking project is ok
    if project_id in [project.id for project in projects]:
        selected = sv.get_project_with_id(project_id)
    else:
        return jsonify({'message': 'MET - Project not found'}), 404
    if selected.dataset_path == '':
        return jsonify({'message': 'MET - The project does not have an associated dataset'}), 422
    else:
        dataset_name = selected.dataset_path

    # Checking dataset is available and computing the distributions
    file_path = os.path.join(app.config['UPLOAD_FOLDER'], dataset_name)
    if os.path.isfile(file_path):
        try:
            answer = met.get_metrics(file_path, class_name, sensitive_name, positive_outcome, priviledged_group, metcode) 
            if not answer:
                return jsonify({'message': 'MET - Dataset found for does not allow function: wrong attribute'}), 422         
        except:
            return jsonify({'message': 'MET - Dataset found but not processed'}), 422
    else:
        app.logger.error('error: README.md file was not found.')
        return jsonify({'error': 'MET - File not found'}), 400 
    
    # Checking that configuration file is available
    try:
        with open(os.path.join(app.config['UPLOAD_FOLDER'], selected.config_path), 'r', encoding='utf-8') as file:
            file_content = file.read()
    except FileNotFoundError:
        return jsonify({'message': 'MET - File not found'}), 404  
    except SyntaxError:
        print("Invalid syntax in the file. Make sure the file contains a valid Python dictionary.")
        return jsonify({'message': 'MET - Corrupted file'}), 422    
    
    # Dropping the results in the json file
    try:
        dictionary_data = ast.literal_eval(file_content)
        list_to_print = []
        for key in answer[positive_outcome][priviledged_group].keys():
            list_to_print.append([key,answer[positive_outcome][priviledged_group][key]])
        print(metric,class_name,sensitive_name,priviledged_group,positive_outcome,list_to_print)

        dictionary_data['Results']['metrics']= [metric,class_name,sensitive_name,priviledged_group,positive_outcome,list_to_print]     
        with open(os.path.join(app.config['UPLOAD_FOLDER'], selected.config_path), 'w', encoding='utf-8') as file:
            json.dump(dictionary_data, file, default=str)
        return jsonify({'message':f'{metric} computed for class {class_name} and sensitive attribute {sensitive_name}.'}), 200
        #jsonify({'Message': 'Results uploaded'}), 200 #Remove this with frontend
    except IOError:
        print("Error saving dictionary to file.")
        return jsonify({'message': 'DIST - Internal Server Error'}), 500
    

@app.route('/api/get_options',methods=['POST'])
def get_options():
    # Checking if database is available
    try:
        projects = sv.get_projects()
    except:
        return jsonify({'message': 'MET - Unavailable database'}), 503 
    # Getting API data
    project_id = request.json.get('project_id')
    selected_attribute = request.json.get('attribute')
    # Checking project is ok
    if project_id in [project.id for project in projects]:
        selected = sv.get_project_with_id(project_id)
    else:
        return jsonify({'message': 'MET - Project not found'}), 404
    if selected.dataset_path == '':
        return jsonify({'message': 'MET - The project does not have an associated dataset'}), 422
    else:
        dataset_name = selected.dataset_path
    # Checking dataset is available and getting the options
    file_path = os.path.join(app.config['UPLOAD_FOLDER'], dataset_name)
    if os.path.isfile(file_path):
        try:
            answer = met.get_values(file_path, selected_attribute) 
            if not answer:
                return jsonify({'message': 'MET - Dataset found for does not allow function.'}), 422         
            return jsonify(answer)
        except:
            return jsonify({'message': 'MET - Dataset found but not processed'}), 422
    else:
        app.logger.error('error: README.md file was not found.')
        return jsonify({'error': 'MET - File not found'}), 400 

#---------------------------------- Users management in database APIs
