# -------------------------------------------------------------------------------
# MIT License
   
# Copyright (c) 2024 University College Cork (UCC)
    
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
    
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
   
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
     
      
# SPDX-License-Identifier: MIT
      
# Contributors:
#    Panagiotis Kyziropoulos - Author
#    Dimitrios Bikoulis - Co-author
    
# ------------------------------------------------------------------------------- 

from app.models import db, Project, User
from app import app
from datetime import datetime
import random
import string
import json
import os

# -------- Tools -------- 

# Function: generate_string
# Description: Generats a random id string
def generate_string(length=32):
    return ''.join(random.choices(string.ascii_letters + string.digits, k=length))

# Function: allowed_file
# Description: checks if the filename has the correct extension
def allowed_file(filename, extention):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in extention


# ----------------------------------------------------------------------------------------------------
# PROJECT functions

# Function: create_configuration_file
# Description: create a json configuration file for a project
def create_configuration_file(data, path):
    # Write data to the JSON file
    with open(path, 'w') as json_file:
        json.dump(data, json_file)
    return

# Function: create_configuration_file
# Description: create a json configuration file for a project
def delete_files(folder,config_path,data_path):
    if data_path !=' ':
        data_filepath = os.path.join(folder,data_path)
        try: 
            os.remove(data_filepath)
        except:
            print(f"File '{data_filepath}' not found.")
    else:
        print('No data file associated')

    os.remove(os.path.join(folder,config_path))
    print(os.path.join(folder,config_path))
    return

# Function: add_project
# Description: Adds a new project to database
def add_project(name, description, filefolder, dataset=False):
    # get timestamp in string format
    now=datetime.now().strftime("%d/%m/%Y, %H:%M:%S")
    # creating project data
    new_project_id = generate_string()   
    config_filename = new_project_id+"_conf.json"
    config_filepath = os.path.join(filefolder, config_filename)
    if dataset:
        dataset_filename = new_project_id+"_data.csv"
    else:
        dataset_filename = ' '
    # add new project to db
    project = Project(id=new_project_id, name=name, config_path=config_filename, dataset_path=dataset_filename, owner=1, date=now)
    db.session.add(project)
    db.session.commit()
    # Data to be written to the JSON file
    data = {
        "Project_Name": name,
        "Creation_Date": now,
        "Dataset_Filepath": dataset_filename,
        "Description": description,
        "Results":{}
    }
    # create project's configuration file 
    config_filepath=create_configuration_file(data,config_filepath)
    return new_project_id

# Function: update_path
# Description: Modifies the configuration file path in the Project object
def update_paths(project_id,dataset_filepath):
    project = Project.query.get(project_id)
    now=datetime.now().strftime("%d/%m/%Y, %H:%M:%S")
    if project:
        project.dataset_path = dataset_filepath
        project.date = now
        db.session.commit()
        return 1
    else:
        return 0

def update_configuration_file(project_id,dataset_filename):
    try:
        project = Project.query.get(project_id)
        config_path = os.path.join(app.config['UPLOAD_FOLDER'],project.config_path)
        with open(config_path) as f:
            s = f.read()
        data = json.loads(s)
        data["Dataset_Filepath"] = project_id+"_data.csv"
        # data = {
        #     "Project_Name": project.name,
        #     "Creation_Date": project.date,
        #     "Dataset_Filepath": dataset_filename,
        #     "Description": project.description,
        #     "Results":{}
        # }
        with open(config_path, 'w') as json_file:
            json.dump(data, json_file)
        return 1
    except:
        print('Project not found')
        return 0
    
# Function: remove_project
# Description: Removes a project from database using the ID
def remove_project(project_id,filefolder):
    project = Project.query.get(project_id)
    if project:
        # Removal of storing files
        try:
            delete_files(filefolder,project.config_path,project.dataset_path)
        except:
            print("Files not deleted")
        # Removal of the object in the database
        db.session.delete(project)
        db.session.commit()
        return 1
    else:
        return 0

# Function: get_projects
# Description: Returns all the projects inside the Database
def get_projects():
    return Project.query.all()

# Function: get_project_with_id
# Description: Returns a project using the ID
def get_project_with_id(project_id):
    return Project.query.get(project_id)

# Function: get_project_with_id
# Description: Returns a project using the ID
def upload_file_to_project(project_id,file):
    if file and allowed_file(file.filename, app.config['ALLOWED_EXTENSIONS']):
        if len(file.read()) > app.config['MAX_CONTENT_LENGTH']:
            return 413
        project = Project.query.get(project_id)
        if project.dataset_path == ' ':
            try:
                dataset_filename = project_id+"_data.csv"
                update_paths(project_id,dataset_filename)
            except:
                return 401
        else:
            return 422
        try:
            file.seek(0)
            dataset_filepath = os.path.join(app.config['UPLOAD_FOLDER'],dataset_filename)
            file.save(dataset_filepath)
            return 200
        except:
            return 400


    
# ----------------------------------------------------------------------------------------------------
# USER functions

# Function: add_user
# Description: Adds a new user to database
def add_user(name, password):
    #HASH the password!
    new_user = User(id=generate_string(),username=name,password=password)
    db.session.add(new_user)
    db.session.commit()

# Function: create_hash
# Description: It encondes the passwords with a high level hash function for storing in the database
def create_hash(original: str):
    hashed_string = original
    return hashed_string

# Function: check_hash
# Description: for log-in, it checks if the given password coincides with the one stored for the user
def check_hash(text,hash):
#    if create_hash(text) == hash:
    if text == hash:
        return True
    else:
        return False
    
# Function: delete_user
# Description: It removes a given user from database
def delete_user(id):
    ### There should be an admin user that it is allowed to do this...... We shall see
    return None

# Function: get_users
# Description: It returns all the users stored in the database
def get_users():
    return User.query.all()