# -------------------------------------------------------------------------------
# MIT License
   
# Copyright (c) 2024 University College Cork (UCC)
    
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
    
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
   
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
     
      
# SPDX-License-Identifier: MIT
      
# Contributors:
#    Panagiotis Kyziropoulos - Author
#    Dimitrios Bikoulis - Co-author
    
# ------------------------------------------------------------------------------- 

# detection/metrics.py
from typing import Union
import numpy as np
import pandas as pd
from app.tools import type_check,_check_attribute,_check_value

# ------------------- Public Functions --------------------

def get_metrics(file_path: str, class_name: str, sensitive_name: str, positive_outcome: str, privileged_group: str, metric: str):
    try:
        dataset = pd.read_csv(file_path)
        if metric == 'stats':
            results = stats(dataset, class_name, sensitive_name, positive_outcome)
        elif metric == 'parity':
            results = statistical_parity(dataset, class_name, sensitive_name, positive_outcome, privileged_group)
        elif metric == 'disimp':
            results = disparate_impact(dataset, class_name, sensitive_name, positive_outcome, privileged_group)
        else:
            print('Metric not valid')
            return False
        return results
    except:
        return False
    
def get_values(file_path: str, attribute: str):
    try:
        dataset = pd.read_csv(file_path)
        _check_attribute(dataset,attribute)
        return list(dataset[attribute].unique())
    except:
        return False

# ------------------- Public Functions --------------------

@type_check
def stats(data: pd.DataFrame, class_attribute: str, sensitive_attribute: str, positive_outcome: Union[int, float, str] = None) -> dict:
    # return object
    result={}
    # check validity of features
    _check_attribute(data,class_attribute)
    _check_attribute(data,sensitive_attribute)
    # combute features unique values.
    class_values=list(data[class_attribute].unique())
    sensitive_values=list(data[sensitive_attribute].unique())
    # get positive outcome value
    positive_outcomes=_check_value(class_values,[positive_outcome])
    #iterate through outcomes (class values) 
    for outcome in positive_outcomes:
        temp={}
        #iterate through sensitive values
        for value in sensitive_values:
            # compute combinatorial probability of sensitive value
            P_C=len(data[(data[sensitive_attribute] == value) & (data[class_attribute] == outcome)])        
            # compute marginal probabilitiy of sensitive value
            P=len(data[data[sensitive_attribute] == value])
            if isinstance(value,np.int64):
                value=int(value)
            # compute probability of outcome
            if (P==0):
                temp[value]=0
            else:
                temp[value]=np.abs(P_C/P)
        if isinstance(outcome,np.int64):
            outcome=int(outcome)
        result[outcome]=temp
    return result


@type_check
def statistical_parity(data: pd.DataFrame, class_attribute: str, sensitive_attribute: str, positive_outcome: Union[int, float, str] = None, privileged_group: Union[int, float, str] = None) -> dict:
    # return object
    result={}
    metric=0
    # get probablities
    probabilities=stats(data, class_attribute, sensitive_attribute)
    #get class and sensitive values 
    class_values=list(data[class_attribute].unique())
    sensitive_values=list(data[sensitive_attribute].unique())
    # get privileged group value
    privileged_groups=_check_value(sensitive_values,[privileged_group])
    # get positive outcome value
    positive_outcomes=_check_value(class_values,[positive_outcome])
    #iterate through positive outcomes (class values)  
    for outcome in positive_outcomes:      
        temp={}
        #iterate through privileged groups (base)
        for value1 in privileged_groups:
            base={}
            #iterate through sensitive values
            for value2 in sensitive_values:
                if isinstance(value1,np.int64):
                    value1=int(value1)
                if isinstance(value2,np.int64):
                    value2=int(value2)
                # compute statistical parity differences
                base[value2]=probabilities[outcome][value1]-probabilities[outcome][value2]
                # compute single metric if possible
                if ((len(positive_outcomes)==1) and (len(privileged_groups)==1) and (value1!=value2)):
                    metric=base[value2]
            temp[value1]=base
        if isinstance(outcome,np.int64):
            outcome=int(outcome)
        result[outcome]=temp
        result["metric"]=metric
    return result


@type_check
def disparate_impact(data: pd.DataFrame, class_attribute: str, sensitive_attribute: str, positive_outcome: Union[int, float, str] = None, privileged_group: Union[int, float, str] = None) -> dict:
    # return object
    result={}
    metric=0
    # get probablities
    probabilities=stats(data, class_attribute, sensitive_attribute)
    #get class and sensitive values 
    class_values=list(data[class_attribute].unique())
    sensitive_values=list(data[sensitive_attribute].unique())
    # get privileged group value
    privileged_groups=_check_value(sensitive_values,[privileged_group])
    # get positive outcome value
    positive_outcomes=_check_value(class_values,[positive_outcome])
    #iterate through positive outcomes (class values)  
    for outcome in positive_outcomes:
        temp={}
        #iterate through privileged groups (base)
        for value1 in privileged_groups:
            base={}
            #iterate through sensitive values
            for value2 in sensitive_values:
                if isinstance(value1,np.int64):
                    value1=int(value1)
                if isinstance(value2,np.int64):
                    value2=int(value2)
                # compute disparate impact
                base[value2]=probabilities[outcome][value2]/probabilities[outcome][value1]
                # compute single metric if possible
                if ((len(positive_outcomes)==1) and (len(privileged_groups)==1) and (value1!=value2)):
                    metric=base[value2]
            temp[value1]=base
        if isinstance(outcome,np.int64):
            outcome=int(outcome)
        result[outcome]=temp
        result["metric"]=metric
    return result

